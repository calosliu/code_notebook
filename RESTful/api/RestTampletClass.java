// RestTemplate Usage
@Service
public class LogisticsService {
	private RestTemplate restTemplate;

	@Value("${service.logistics.baseurl}")
	private String baseUrl;
	private String suffix = "logistics";

	public LogisticsService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getLogistics() {
		return restTemplate.getForEntity(baseUrl + suffix, String.class)
		.getBody();
	}
}

//baseURL + suffix = RESTful url address