// SwaggerConfig app
@Configuration
@EnableSwagger2
public class SwaggerConfig {
  @Bean
  public Docket documentation() {
    return new Docket(DocumentationType.SWAGGER_2).protocols(Sets.newHashSet(protocol)).select()
           .apis(RequestHandlerSelectors.any()).paths(regex(".*api.*")).build().pathMapping("/")
           .useDefaultResponseMessages(true).ignoredParameterTypes(Error.class);
  }
}