@Component
public class CustomerHealthIndicator implements HealthIndicator {
  @Override
  public Health health() {
    return Health.up().withDetail("reachable", "true").build();
  }
}