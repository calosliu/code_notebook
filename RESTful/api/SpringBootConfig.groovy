// used for gradle itself
buildscript{

  ext{
    springBootVersion='1.5.7.RELEASE'
  }

  repositories{
    mavenCentral()
  }

  dependencies{
    classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
  }
}

// use java, eclipse and spring boot
applyplugin:'java'
applyplugin:'eclipse'
applyplugin:'org.springframework.boot'

group='com.guild'
version='0.0.1-SNAPSHOT'
sourceCompatibility=1.8

repositories{
  mavenCentral()
}

// gradle setting
dependencies {
	compile('org.springframework.boot:spring-boot-starter-actuator')
	// compile('org.springframework.boot:spring-boot-starter-security')
  compile('org.springframework.boot:spring-boot-starter-web')
	testCompile('org.springframework.boot:spring-boot-starter-test')
	testCompile('org.springframework.security:spring-security-test')

// swagger
	compile "io.springfox:springfox-swagger2:2.7.0"
	compile "io.springfox:springfox-swagger-ui:2.7.0"

// dozer	
	compile("net.sf.dozer:dozer:5.4.0") {
		exclude group: "org.slf4j"
	}
}
