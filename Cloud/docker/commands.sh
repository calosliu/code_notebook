# run traefik:1.7 with endpoints 80/443/10080
docker run -d --restart unless-stopped --log-opt max-size=10m \
   -v traefik-letsencrypt:/etc/traefik/acme \
   -v traefik-tmp:/tmp \
   -v /var/run/docker.sock:/var/run/docker.sock:ro \
   --net freshrss-network \
   -p 80:80 \
   -p 443:443 \
   -p 10080:10080 \
   --name traefik traefik:1.7 --docker \
   --loglevel=info \
   --entryPoints='Name:http Address::80 Compress:true Redirect.EntryPoint:https' \
   --entryPoints='Name:https Address::443 Compress:true TLS TLS.MinVersion:VersionTLS12 TLS.SniStrict:true TLS.CipherSuites:TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA' \
   --entryPoints='Name:rss Address::10080 Compress:true TLS TLS.MinVersion:VersionTLS12 TLS.SniStrict:true TLS.CipherSuites:TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA' \
   --defaultentrypoints=http,https,rss --keeptrailingslash=true \
   --acme=true --acme.entrypoint=https --acme.onhostrule=true --acme.tlsChallenge \
   --acme.storage=/etc/traefik/acme/acme.json --acme.email=whulc@outlook.com

# run freshrss with network freshrss-network
docker run -d --restart unless-stopped --log-opt max-size=10m \
   -v freshrss-data:/var/www/FreshRSS/data \
   -v freshrss-extensions:/var/www/FreshRSS/extensions \
   -e 'CRON_MIN=4,34' \
   --net freshrss-network \
   --label traefik.port=80 \
   --label traefik.frontend.rule='Host:rss.congliu.dev' \
   --label traefik.frontend.headers.forceSTSHeader=true \
   --label traefik.frontend.headers.STSSeconds=31536000 \
   --name freshrss freshrss/freshrss

# create new docker volume
docker volume create pgsql-data

# run postgresql
docker run -d --restart unless-stopped --log-opt max-size=10m \
   -v pgsql-data:/var/lib/postgresql/data \
   -e POSTGRES_DB=freshrss \
   -e POSTGRES_USER=freshrss \
   -e POSTGRES_PASSWORD=post_P@ssword \
   --net freshrss-network \
   --name postgres postgres

docker network connect freshrss-network postgres
