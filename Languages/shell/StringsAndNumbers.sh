# return word if param not assigned value
${param:-word}

# assign word to param and return
${param:=word}

# print value to std_error if param is not defined
${param:?word}

# return word if param is not null
${param:+word}

# expansion to all environment virables
${!BASH*}
${!BASH@}

# param's length
${#param}

# extract portion of a string
# if param is @, this will show the args
${param:offset[:length]}

# remove the pattern from front
# #:shortest ##:longest
${param#[#]pattern}

# remove the pattern from end
# %:shortest %%:longest
${param%[%]pattern}

${param/[/#%]pattern/string}