# defination, supported after bash 2
declare -a array
array[0]=0
array=(0 1)
array=([0]=0 [1]=1)

# all elements in an array
array[*]  # "array[*]" adds "" for whole elements
array[@]  # "array[@]" adds "" for each elements

# length
${#array[@]} # length of array
${#array[n]} # length of n-th element

${!array[@]} # all subscripts that exist

# append elements
array+=(a b c)

# delete array
unset array
# delete element
unset 'array[n]'

# value without subscript points to first element
array=a # array[0] will be a

# order array
for i in "${array[@]}"; do echo ${i}; done | sort

# associative array
declare -A array
array["a"]="a"