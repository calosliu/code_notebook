# syntactic error
lose punctuation
lose command

# unexpected expansion
use "" to quote the variable

# logical error
# Incorrect conditional expressions
if / while / until

# “Off by one” errors
# start from
0 / 1
# and end with
n / n+1

# Unanticipated situations
file not existed


# -----------------------------------------------
# ----- verify assumptions when programming -----
# 0
cd $dir_name
rm *
# 1
cd $dir_name && rm *
# 2
[[ -d $dir_name ]] && cd $dir_name && rm *
# 3
using script with 

# ------------------------------
# ----- input verification -----
