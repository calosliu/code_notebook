# number of arguments
$#

# pid of last process
$!

# exit code of last process
$?

# list of arguments
$1 $2 $3 ... $9
${10} ${11}

# iterate arguments
shift # move args to left by one

# name or path
$0 # script path
basename $0 # script file name
$FUNCNAME # function name

# passing args list to lower script
$@ # add "" to each args
$* # add "" to the whole args