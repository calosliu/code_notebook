# Combination Commands
# 1. group commands
{ command1; command2; command3; }
# 2. subshell
(command1; command2; command3;)

"This can be used with pipe"
(ls; ll) > output.txt


# Process substitution
read a b c < <(ls -l | tail -n +2)
<(command) # create standard output
>(command) # accept standard input

# Trap
trap argument signal [signal...]

# Way to create temporary files
file=$(mktemp /temp/folder/xxxx.xxxx)
file="/temp/folder/$(basename $0).$$.$RANDOM"
# file name + pid + random number

# named pipeline
mkfifo pipeline
input > pipeline
pipeline > output
