# used to get file from ftp
FTP_SERVER=ftp.gnu.org
FILE_PATH=/gnu/diction
FILE_NAME=diction-1.11.tar.gz
ftp -n <<- _EOF_
	open $FTP_SERVER
	user $anonymous
	cd $FILE_PATH
	hash
	get $FILE_NAME
	bye
_EOF_