# announce this is a bash script
#!/bin/bash

# used as here document
# <<- will ignore space before line and << won't
command <<- _EOF_
  echo "some text"
_EOF_

# functions in shell
# !!functions must be announced before using
function func {
  commands
  return
}

# or
func () {
  commands
  return
}

# local variables
func () {
  local local_variables
}

# branch
# if followed by multiple conditions, use last one
if [ false; true; ]; then
  echo "true"
elif [ true; false; ] then
  echo "false"
else
  echo "default"
if

# exit status: $?
# 0 for success, other for error
echo $?

# read data
# if only read without variable name
# REPLY will be assigned the input
# read
# $REPLY
read input