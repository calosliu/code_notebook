# While
while [ expression ]; do
  commands
  continue / break
done


# Until
until [ expression ]; do
  commands
done


# Case
case word in
    pattern [| pattern]...) commands 
    ;;  # ;;& in bash 4, can continue
    ...
esac


# For
for var in {set}; do commands; done

for (( i=1; i<5; i=i+1 )); do
  commands
done