/**
 * Basic types:
 * Boolean, number, string
 * Array, tuple, Enum
 * Any, Object
 * void = undefined + null
 * never : function never get end point
 */

var t0: Array<number> = [1, 2, 3]; // function working
let t1: string = 'name'; // block working
const t2: number = 2; // unchangeable
let t3: [number, string]; // tuple suger for js
t3 = [1, 'haha'];
enum Color {
  RED,
  'Green',
} // Enum declaration
Color.Green;
let color = Color[1];

let NULL: void = null;

function neverEnd(): never {
  throw new Error('wrong');
}

/**
 * Type assertion:
 * type cast
 */

let str: any = 'some thing';
let len: number = (<string>str).length;
let len1: number = (str as string).length;

/**
 * Declaration
 * -----------
 * var:
 * hoisted to be undefined
 * function-scope
 * captured when function is actually called
 * redeclaration
 *
 * let:
 * block-scope
 * shadowing outter scope
 *
 * const:
 * Immutable, cannot be re-assigned
 * members can be Readonly
 *
 * --------------
 * Destructuring:
 * /////big piece/////
 */

console.log(c); // "undefined"
var c = 5;

// console.log(d);                            // error
let d = 5;

for (var i = 0; i < 5; i++) {
  for (var i = 5; i < 10; i++) {
    // no shadowing
    console.log(i);
  }
}

function var_scope() {
  for (var i = 0; i < 5; i++) {
    setTimeout(function () {
      console.log(i); // will all be '5'
    }, i * 100);
  }
}

/**
 * Interface
 * ---------
 * duck typing - only check if requirement satisfied, no need to be exactly the same
 * Optional properties
 * Readonly properties (ReadonlyArray<T>)
 * Excess property checks
 * Function types
 * Indexable Types: dictionary
 * Extends interface (multiple)
 * Hybrid types
 * Extending Classes ??? (inherit all properties including private ones, how to access?)
 */

// Optional properties
function t_typeCheck(person: { firstName: string; age?: number }): string {
  return 'Hello, ' + person;
}

function t_typeCheck2(person: Person): string {
  console.log(person.props);
  return person.firstName;
}

// duck typing
let you = { firstName: 'abc', lastName: 'def', sex: 'boy' };
t_typeCheck(you); // only check if 'you' has a property named 'firstName'
t_typeCheck2(you); // check if 'you' has both 'firstName' and 'lastName'
t_typeCheck2({ firstName: 'a', lastName: 'b', sex: 'c' }); // has [props: string]: string

// Readonly properties
interface Person {
  readonly firstName: string;
  readonly lastName: string;
  [props: string]: string;
}

// Excess property checks
t_typeCheck({ firstName: 'ha', lastName: 'ha', sex: 'girl' } as Person); // has to convert

// Function types
interface interface_types {
  (a: string, b: string, c: number): boolean;
}

let func: interface_types = function (c, b, a) {
  return a > 0;
};

// Indexable Types
interface StringArray {
  [index: string]: Person;
  [indexer: number]: Person;
  readonly lastone: Person;
}
let myArray: StringArray = { a: you, b: you, 1: you, lastone: you };

// Extends interface
interface Boy extends Person {
  // new (a: number): string;
  // func(): void;
}

// Hybrid types
interface Hybrid_interface {
  prop1: number;
  prop2: string;
  printOut(): void;
  getProp1(): number;
  [props: number]: string;
  // [prop: string]: string;                 // cannot working with other types
}

// Extend class
class GrandFather {
  private lastName: string;
  familyName(): string {
    return this.lastName;
  }
}

interface family extends GrandFather {
  familyName(): string;
}

class Father extends GrandFather implements family {
  constructor() {
    super();
  }

  familyName(): string {
    return super.familyName();
  }
}

/**
 * Class (almost the same as java)
 * -----
 * Implementing interface (has to declare signature explicitly)
 *
 */

class t_Student implements Person {
  fullName: string;
  firstName: string;
  lastName: string;
  [props: string]: string;
  constructor(
    public first: string,
    public middleInitial: string,
    public last: string
  ) {
    this.firstName = first;
    this.lastName = last;
    this.fullName = this.firstName + ' ' + middleInitial + ' ' + this.lastName;
  }
}

let t_user = new t_Student('Jane', 'M.', 'User');

/**
 * Functions
 * ---------
 * Capture => !!! important feature!
 * Function type
 * Inferring the types
 * Optional and Default parameters
 * Rest parameters
 * This => !!! important feature!
 */

//== Capture && type
let t_capture = 'a';
function func_capture(x: string, y: number): string {
  return x + y + t_capture;
}

//== Inferring the types
let func_add: (
  firstParameter: string,
  secondParameter: number
) => string = function (x: string, y: number) {
  return x + y;
};

//== Optional and Default parameters
// Below function has type:
// (x?: number, y?: number) => number
function func_opt_def(x = 10, y?: number) {
  if (y) {
    return x + y;
  }
  return x;
}

//== Rest parameters
function func_rest(x: number, ...rest: number[]): number {
  return x + rest.length;
}

//== This
function func_this(str: string): void {
  console.log('just hello');
}
// this:
func_this('world');
// desugers to:
func_this.call(window, 'world');
//func_this.call(undefined, "world") when EC5 strict mode

var person = {
  name: 'name',
  hello: function (str: string): void {
    console.log(str + 'hello');
  },
};
// this:
person.hello('world');
// desugars to:
person.hello.call(person, 'world');

// Using bind to attach a this value
var person_bind = {
  name: 'name',
  hello: function (str: string): void {
    console.log(this.name + 'says hello ' + str);
  },
};
var boundHello = function (str) {
  return person_bind.hello(str);
};

//== Overloads
// These two overloads won't be in compiled js file
function func_overload(x: number): { name: string; classes: string[] };
function func_overload(x: string): { name: string; classes: string[] };
// implementation
// still need to judge the type mannully
function func_overload(x): any {
  if (typeof x == 'number') {
    return { name: 'abc', classes: ['abc', 'abc'] };
  }
}

/**
 * Generics
 * ========
 * Generics types
 *
 */

function func_generics<T>(params: T): T {
  return params;
}

let t_generics = func_generics<string>('String');

// type argument inference
let t1_generics = func_generics('string');

// Generics Types
let func_gen_type: <T>(arg: T) => T = func_generics;
let func_gen_type1: { <U>(arg: U): U } = func_generics; // ignature of object literal type

// function interface
interface GenericsInterface<U> {
  <T>(arg: T): T;
  (arg: U): U;
}
function func_gen<U>(params: U): U {
  return params;
}
let generics_t: GenericsInterface<number> = func_gen;

// Generics Classes
class GenericsClass<T, U> {
  // static val: T;           //static value cannot be generics
  value: T;
  add: (x: U, y: T) => T;
}

// Generics Constraints
interface LengthInterface {
  length: number;
}

function func_gen_array<T extends LengthInterface>(params: T): T {
  console.log(params.length);
  return params;
}
// Type parameters in generics constraints
function getProperty<T, K extends keyof T>(t: T, k: K) {
  return t[k];
}

/**
 * Enums
 * =====
 * Numeric
 * string
 * Literal enum expression
 * Computed
 * Reverse mapping
 */

//  numeric
enum KEY {
  // T = getAValueFromFunction(), cannot come before no initial one
  A, // A is 0
  B, // B is 1
  C = 5,
  D = 1 << 2,
  E = C | D,
}

// string, no auto increcement
enum K {
  A = 'A',
  B = 'B',
  C = A,
}

// literal enum expression
enum enum_literal {
  NONE,
  A = 1,
  B = A,
  C = A | B,
  // D = "D" //cannot work with expression above
  E = A >> 2,
  F = E * 10,
}

// computed
enum enum_computed {
  A = K.A.length,
  B = 'abc'.length,
}

/**
 * Type inference
 * ===============
 * Initializing numbers/string/parameter default value/function return type
 * Best common type -
 */

class Animal {
  commondo: () => void;
}
class Snake extends Animal {
  snakedo: () => {};
}
class Elephent extends Animal {
  elephentdo: () => {};
}
class Dolphine extends Animal {
  dolphinedo: () => {};
}

//  Best Common Type
let ani = [new Snake(), new Elephent(), new Dolphine()]; // no common type
let ani_: Animal[] = [new Snake(), new Elephent(), new Dolphine()]; // use super class

// Contextual Type
window.onmousedown = function (mounseEvent: any) {
  console.log(mounseEvent.button);
};

/**
 * Type Compatibility
 * ==================
 * base structural subtyping
 * Subtype vs Assignment ??? spec
 */

//  structural subtyping
interface A_name {
  name: string;
}

class the_name {
  name: string;
}

let a_name: A_name = the_name; // OK, because of the js anonymous object way

// functions
function comp_func_A(params: string) {}

function comp_func_B(params: string, params2: number) {}

let comp_func_a = comp_func_A;
let comp_func_b = comp_func_B;
comp_func_b = comp_func_a;
// comp_func_a = comp_func_b;  // not allowed because A doesn't supply type of params2

/**
 * Advanced types
 * ==============
 * ! Intersection type
 * ! Union type
 * Type Guards and Differentiating Types
 * User defined type Guards(param is type)
 * typeof type guard (primitives)
 * instanceof type guard (use constructor function)
 * Nullable types: null, undefined
 * - Type guards and type assertions
 * ! Type alias, used for tuple and union (prefer interface.)
 * ? string literal types
 * numeric literal types
 * ! Discriminated Unions (tagged unions or algebraic data types)
 * Exhaustiveness checking
 * ! Polymorphic this types
 * index types
 * !?Mapped type
 */

//  Intersection type: combines all members
let inter_type: Snake & Elephent;

// Union type: one of many
function getUnionType(): Snake | Elephent | Dolphine {
  return;
}
let union_type: Snake | Elephent | Dolphine = getUnionType();
union_type.commondo();
// union_type.snakedo();  //Error, not common

// Type Guards and Differentiating Types
function unionTypeDo(union_type: Snake | Elephent | Dolphine): void {
  if ((<Snake>union_type).snakedo) {
    (<Snake>union_type).snakedo();
  } else if ((<Elephent>union_type).elephentdo) {
    (<Elephent>union_type).elephentdo();
  } else {
    (<Dolphine>union_type).dolphinedo();
  }
}

// User-Defined Type Guards   parameterName is Type
function getSnakeType(params: Snake | Elephent | Dolphine): params is Snake {
  return (<Snake>params).snakedo !== undefined;
}

// typeof type guards(only primitive types)
function typeGuard(params: number | string): string {
  if (typeof params === 'number') {
    return Array(params).join(' ');
  } else if (typeof params === 'string') {
    return params;
  }
}

// Nullable types
// --strictNullChecks exclude null,undefined
// optional parameter will include undefined
let null_type: string | null;

// - Type guards
function remove_null(param: string) {
  if (param == null) {
    // eliminate null
    return 'null';
  }
  // or
  return param || 'null';
}

// - type assertions
function type_assert(params: string | null) {
  function nested_func(param: string) {
    return params!.charAt(0) + param;
  }
  params = params || 'params';
  return nested_func(params);
}

// Type alias
type string_name = string;
type function_name = () => string;
type linkedList<T> = T & { next: linkedList<T> };

type Alias = {
  name: string;
};
interface Interface {
  name: string;
}

declare function type_alias(): Alias;
declare function interfaces(): Interface;

// discriminant unions
interface Square {
  kind: 'square'; // discriminant
  size: number;
}
interface Rectangle {
  kind: 'rectangle';
  width: number;
  height: number;
}
interface Circle {
  kind: 'circle';
  radius: number;
}
type Shape = Square | Rectangle | Circle;

function discriminant_union(params: Shape): number {
  switch (params.kind) {
    case 'square':
      return params.size * params.size;
    case 'rectangle':
      return params.height * params.width;
    case 'circle':
      return params.radius * params.radius; // work with type guard
    default:
      assertNever(params); // check no cases left
  }
}

function assertNever(params: never): never {
  throw new Error('unexpected');
}

// Polymorphic this types
class Calculator {
  public constructor(protected value: number = 0) {}

  public getValue() {
    return this.value;
  }
  public add(num: number) {
    this.value += num;
    return this;
  }
  public multiple(num: number) {
    this.value *= num;
    return this;
  }
}
new Calculator(1).add(1).multiple(2).getValue();

class SintificCalculator extends Calculator {
  public constructor(value: number) {
    super(value);
  }

  public add(num: number) {
    this.value -= num;
    return this;
  }
  public sin() {
    this.value = Math.sin(this.value);
    return this;
  }
}

// index types
// index type query operator: typeof T
// indexed access operator: T[K]
function index_type<T, K extends keyof T>(t: T, k: K[]): T[K][] {
  return k.map((n) => t[n]);
}

interface Map_type<T> {
  [keys: string]: T;
}

let map_types: Map_type<number> = { man: 3, women: 5, girl: 5 };

let keys: keyof Map_type<number> = 'ma';

console.log(map_types[keys]);

/**
 * Symbols  !!!!!
 * =======
 * immutable and unique
 *
 */

//  immutable and unique
let sym1 = Symbol();
let sym2 = Symbol();
console.log(sym1 == sym2); //false

// object property and class members
const symbol = Symbol('getSymbol');
class SymbolClass {
  [symbol]() {
    return 'ha';
  }
}
let symbol_object = {
  [symbol]: 'abc',
};
