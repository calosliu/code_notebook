// package
package com.mypackage.kotlin

// imports
import kotlin.text.*

// main function
fun main(args: Array<String>): Unit {

    // variable
    // val immutable
    // var mutable
    // lateinit var
    val fooVal = 10
    var fooVar = 10
    lateinit var fooLateVar: Int
    fooLateVar = 10

    // string
    // \n (translate
    // """ """ (raw string
    // "$variable" (template string
    var foo1String = "A string"
    var foo2String = "A string\n second string"
    var fooRawString = """
        fun testFun(): Unit {
            println("only a stirng")
        }
        """
    var fooTemplateString = "$foo1String and ${foo2String}"

    // null
    // variable: Type? (variable can be null
    // variable?.member (access to variable which can be null
    // variable ?: default (default will be used if variable is null
    var fooNullableString: String? = null
    fooNullableString?.length
    fooNullableString ?: "default value"

    // function
    // parameter default value
    //
    fun fooFun(name: String = "default name"): String {
        return "Test $name"
    }
    println(fooFun())
    println(fooFun("new name"))
    println(fooFun(name = "new name"))


}