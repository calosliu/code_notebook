class ParameterDemo(object):
    """docstring for ParameterDemo"""

    def __init__(self):
        super(ParameterDemo, self).__init__()

    def test(self):
        self.accept_single_param('single_param')
        self.accept_multiple_param('multiple_params_1', 'multiple_params_2')
        self.accept_multiple_param(*('a', 'b'))
        self.accept_dict_param(dict1='dict1', dict2='dict2')
        # self.accept_limited_dict_param(dict1='dict1', dict2='dict2')
        self.accept_limited_dict_param('dict1', dict3='dict3')
        self.accept_combination_in_order('p1', p4='p4', p6='p6', p7='p7')

    def accept_single_param(self, single_param):
        print(single_param)

    def accept_multiple_param(self, *multiple_params):
        for n in multiple_params:
            print(n)

    def accept_dict_param(self, **kw):
        for d in kw:
            print(d + ":" + kw[d])

    def accept_limited_dict_param(self, dict1, *, dict2='dict2', dict3):
        print(dict1, dict2, dict3)

    def accept_combination_in_order(self, p1, p2='0', *, p4, p5='p5', **p6):
        print(p1, p2, p4, p5, p6)

ParameterDemo().test()
