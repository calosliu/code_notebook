var j0 = [1, 2, 3];
let j1 = 'name';
const j2 = 1;

function j_greeter(person) {
  return 'Hello, ' + person;
}

var Student = (function () {
  function Student(firstName, middleInitial, lastName) {
    this.firstName = firstName;
    this.middleInitial = middleInitial;
    this.lastName = lastName;
    this.fullName = firstName + ' ' + middleInitial + ' ' + lastName;
  }
  return Student;
})();

function Abc(name, sex) {
  this.name = name;
  this.sex = sex;
}

let j_user = new Student('Jane', 'M.', 'User');
