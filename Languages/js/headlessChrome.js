var modal = {
  browsers: ['ChromeHeadless'],

  customLaunchers: {
    ChromeHeadless: {
      base: 'Chrome',
      flags: ['--headless', '--remote-debugging-port=8080'],
    },
  },
};
