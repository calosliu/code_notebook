describe('A Suite', () => {
  it('contains spec with an expectation', function () {
    expect(true).not.toBe(false);
  });

  describe('Included Matcher', () => {
    var a = 12;
    var b = a;
    var c = 11;
    var string = 'abc';
    var array = ['a', 'b', 'c'];
    var object = {
      a: 'a',
      b: 'b',
    };

    it('The "toBe" matcher compares with ===', () => {
      expect(a).toBe(b);
      expect(a).not.toBe(null);
    });

    it('toEqual', () => {
      expect(a).toEqual(12);
    });

    it('toMatch', () => {
      expect(string).toMatch(/abc/);
      expect(string).not.toMatch('cba');
    });

    it('toBeDefined', () => {
      expect(object.a).toBeDefined();
      expect(object.c).toBeUndefined();
    });

    it('toBeNull', () => {
      expect(null).toBeNull();
    });

    it('toBeTruthy', () => {
      expect(true).toBeTruthy();
      expect(false).toBeFalsy();
    });

    it('toContain', () => {
      expect(array).toContain('a');
      expect(string).toContain('a');
    });

    it('toBeClose/GreaterThan', () => {
      expect(5).toBeLessThan(10);
      expect(10).toBeGreaterThan(5);
    });

    it('toBeCloseTo', () => {
      expect(3.3).toBeCloseTo(2.8, 0);
      expect(3.3).toBeCloseTo(3.34, 1);
    });

    it('toThrow', () => {
      var exception = function () {
        throw 'an exception';
      };
      expect(exception).toThrow('an exception');
    });

    it('toThrowError', () => {
      var error_exception = function () {
        throw new TypeError('an error exception');
      };
      expect(error_exception).toThrowError(TypeError, 'an error exception');
    });

    it('CallBack function to test', () => {
      var foo = function (x, callBack) {
        if (x) {
          callBack();
        }
      };

      foo(false, function () {
        fail('Callback has been called');
      });
    });
  });

  xdescribe('Usseless spec', () => {
    xit('Useless test', () => {});
  });

  describe('Pending spec', () => {
    xit('a test', () => {});
    it('no test body');
    it('declared pending in spec body', () => {
      pending('This is why it is pending');
    });
  });

  describe('A spy', () => {
    var func,
      bar = null;

    beforeEach(() => {
      func = {
        val: null,
        setBar: function (value) {
          bar = value;
        },
        getBar: function () {
          return bar;
        },
        setVal: function (value) {
          this.val = value;
        },
        getVal: function () {
          return this.val;
        },
        fake: function () {
          return 'real implementation';
        },
      };

      spyOn(func, 'setBar');
      spyOn(func, 'getBar').and.returnValues('first', 'second');

      func.setBar(123);
      func.setBar(456, 789);

      spyOn(func, 'setVal').and.callThrough();
      spyOn(func, 'getVal').and.callThrough();
      func.setVal(123);

      spyOn(func, 'fake').and.callFake(function () {
        return 'fake implementation';
      });
    });

    it('tracks spy', () => {
      expect(func.setBar).toHaveBeenCalled();
      expect(func.setBar).toHaveBeenCalledTimes(2);
      expect(func.setBar).toHaveBeenCalledWith(123);
      expect(func.setBar).toHaveBeenCalledWith(456, 789);
      expect(bar).toBeNull();
      expect(func.getBar()).toEqual('first');
      expect(func.getBar()).toEqual('second');
    });

    it('calls.any/count/argsFor/allArgs | all/mostRecent/first | reset', () => {
      var calls = func.setBar.calls;
      expect(calls.any()).toBe(true);
      expect(calls.count()).toBe(2);

      expect(calls.argsFor(0)).toEqual([123]);
      expect(calls.argsFor(1)).toEqual([456, 789]);
      expect(calls.allArgs()).toEqual([[123], [456, 789]]);

      // expect a context
      expect(calls.first()).toEqual(
        jasmine.objectContaining({
          object: func,
          args: [123],
          returnValue: undefined,
        })
      );

      calls.reset();

      expect(calls.any()).toBe(false);
    });

    it('callThrough and stub', () => {
      expect(func.val).toEqual(123);
      expect(func.getVal()).toEqual(123);

      func.setVal.and.stub();
      func.val = null;

      func.setVal(123);
      expect(func.val).toBe(null);
    });

    it('callFake', () => {
      expect(func.fake()).toEqual('fake implementation');
    });
  });

  describe('createSpy', () => {
    var whatAmI;
    var tape;

    beforeEach(() => {
      whatAmI = jasmine.createSpy('whatAmI');

      whatAmI('I', 'am', 'a', 'spy');

      tape = jasmine.createSpyObj('tape', ['play', 'pause', 'stop', 'rewind']);
      tape.play();
      tape.pause();
      tape.rewind(0);
    });

    it('identity()', () => {
      expect(whatAmI.and.identity()).toEqual('whatAmI');
    });

    it('same as spyOn', () => {
      expect(whatAmI).toHaveBeenCalled();
      expect(whatAmI.calls.count()).toEqual(1);
    });

    it('creates spies for each requested function', function () {
      expect(tape.play).toBeDefined();
      expect(tape.pause).toBeDefined();
      expect(tape.stop).toBeDefined();
      expect(tape.rewind).toBeDefined();
    });

    it('tracks that the spies were called', function () {
      expect(tape.play).toHaveBeenCalled();
      expect(tape.pause).toHaveBeenCalled();
      expect(tape.rewind).toHaveBeenCalled();
      expect(tape.stop).not.toHaveBeenCalled();
    });

    it('tracks all the arguments of its calls', function () {
      expect(tape.rewind).toHaveBeenCalledWith(0);
    });
  });

  // Type Matching
  describe('jasmine.any/anything/stringMatching/objectContaining/arrayContaining', () => {
    describe('jasmine.any', () => {
      it('matches any value', () => {
        expect({}).toEqual(jasmine.any(Object));
        expect(12).toEqual(jasmine.any(Number));
      });

      describe('when used with spy', () => {
        it('comparing arguments', () => {
          var func = jasmine.createSpy('func');
          func(12, () => {
            true;
          });
          expect(func).toHaveBeenCalledWith(
            jasmine.any(Number),
            jasmine.any(Function)
          );
        });
      });
    });

    describe('jasmine.anything', () => {
      it('matches anything', () => {
        expect(1).toEqual(jasmine.anything());
      });

      describe('used with spy', () => {
        it('ignore argument', () => {
          var func = jasmine.createSpy('func');
          func(12, () => {
            false;
          });

          expect(func).toHaveBeenCalledWith(12, jasmine.anything());
        });
      });
    });

    describe('jasmine.objectContaining', () => {
      var func;

      beforeEach(() => {
        func = {
          a: 1,
          b: 2,
          bar: 'baz',
        };
      });

      it('matches objects with expect key/value pairs', () => {
        expect(func).toEqual(
          jasmine.objectContaining({
            bar: 'baz',
          })
        );
        expect(func).not.toEqual(
          jasmine.objectContaining({
            c: 37,
          })
        );
      });
    });

    describe('jasmine.arrayContaining', () => {});

    describe('jasmine.stringMatching', () => {});
  });

  describe('custom asymmetry', () => {
    var asymmetricTester = {
      asymmetricMatch: (actual) => {
        var secondValue = actual.split(',')[1];
        return secondValue === 'bar';
      },
    };

    it('dives in deep', () => {
      expect('foo,bar,abc,qqq').toEqual(asymmetricTester);
    });
  });

  // Clock
  describe('jasmine.clock()', () => {
    var func;
    beforeEach(() => {
      func = jasmine.createSpy('func');
      jasmine.clock().install();
    });

    afterEach(() => {
      jasmine.clock().uninstall();
    });

    it('timeout to call synchronously', () => {
      setTimeout(() => {
        func();
      }, 100);

      expect(func).not.toHaveBeenCalled();

      jasmine.clock().tick(101);

      expect(func).toHaveBeenCalled();
    });

    it('interval to call synchronously', () => {
      setInterval(() => {
        func();
      }, 100);

      expect(func).not.toHaveBeenCalled();
      jasmine.clock().tick(101);
      expect(func).toHaveBeenCalledTimes(1);
      jasmine.clock().tick(100);
      expect(func).toHaveBeenCalledTimes(2);
    });

    describe('Mocking Date object', () => {
      it('mocks date object', () => {
        var BaseTime = new Date(2017, 3, 28);

        jasmine.clock().mockDate(BaseTime);

        jasmine.clock().tick(50);

        expect(new Date().getTime()).toEqual(BaseTime.getTime() + 50);
      });
    });
  });

  describe('Asynchronous specs', () => {
    var value;
    describe('Using callbacks', () => {
      beforeEach((done) => {
        setTimeout(() => {
          value = 0;
          done();
        }, 1);
      });

      it('should support async execution of test preparation and expectations', function (done) {
        expect(value).toEqual(0);
        value++;
        expect(value).toBeGreaterThan(0);
        done();
      });

      describe('A spec using done.fail', () => {
        var func = (x, callBack1, callBack2) => {
          if (x) {
            setTimeout(callBack1, 0);
          } else {
            setTimeout(callBack2, 0);
          }
        };

        it('should not call the second callBack', (done) => {
          func(true, done, () => {
            done.fail('Second callback has been called');
          });
        });
      });

      describe('Using promises', () => {
        if (!browserHasPromises()) {
          return;
        }

        beforeEach(() => {
          return soon().then(() => {
            value = 0;
          });
        });

        it('should support async execution of test preparation and expectations', function () {
          return soon().then(function () {
            value++;
            expect(value).toBeGreaterThan(0);
          });
        });
      });
    });
  });

  describe('Using async/await', function () {
    if (!browserHasAsyncAwaitSupport()) {
      return;
    }

    beforeEach(function () {
      return soon().then(function () {
        value = 0;
      });
    });

    it('should support async execution of test preparation and expectations', function () {
      return soon().then(function () {
        value++;
        expect(value).toBeGreaterThan(0);
      });
    });
  });

  describe('long asynchronous specs', function () {
    beforeEach(function (done) {
      done();
    }, 1000);

    it('takes a long time', function (done) {
      setTimeout(function () {
        done();
      }, 9000);
    }, 10000);

    afterEach(function (done) {
      done();
    }, 1000);
  });

  function soon() {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        resolve();
      }, 1);
    });
  }

  function browserHasPromises() {
    return typeof Promise !== 'undefined';
  }

  function getAsyncCtor() {
    try {
      eval('var func = async function(){};');
    } catch (e) {
      return null;
    }

    return Object.getPrototypeOf(func).constructor;
  }

  function browserHasAsyncAwaitSupport() {
    return getAsyncCtor() !== null;
  }
});

