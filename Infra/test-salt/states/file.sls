copy file:
  file.managed:
    - name: /etc/http/conf/http.conf
    - source: salt://apache/http.conf

copy dir:
  file.recurse:
    - name: /var/www
    - source: salt://apache/www

append file:
  file.append:
    - name: /etc/http/conf/http.conf
    - test: text to appeded to conf
