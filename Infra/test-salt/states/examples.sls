install vim:
  pkg.installed:
    - name: vim

remove vim:
  pkg.removed:
    - name: vim
    - require:
      - install vim

create directory:
  file.directory:
    - name: /opt/dir
    - user: root
    - group: root
    - mode: 755

make mysql running:
  service.running:
    - name: mysql
    - enable: True

Clone repo:
  pkg.installed:
    - name: git
  git.latest:
    - name: https://github.com/xxx/xxx
    - rev: develop
    - target: /tmp/repo

Add user bob:
  user.present:
    - name: bob
    - shell: /bin/bash
    - home: /home/bob
    - groups:
      - sudo

execute function:
  module.run:
    - name: service.restart
    - m_name: vsftpd

call command line:
  cmd:run:
    - name: echo "{{ pillar['db_username'] }}"
