"""
    BST
"""


class Node:
    def __init__(self, key, left=None, right=None):
        self.key = key
        self.left = left
        self.right = right


def search(root, key):
    if (root is None) or (key == root.key):
        return root

    if key < root.key:
        return search(root.left, key)

    return search(root.right, key)


def insert(root, key):
    if root is None:
        return Node(key)

    if key < root.key:
        root.left = insert(root.left, key)

    if key > root.key:
        root.right = insert(root.right, key)

    return root


def delete(root, key):
    pass


def iterator(root):
    pass


root = Node(10)

insert(root, 5)
insert(root, 3)
insert(root, 2)
insert(root, 4)
insert(root, 8)
insert(root, 7)
insert(root, 9)
insert(root, 15)


print(root.key)

print(str(search(root, 8).key) + " " + str(search(root, 3).key))

print(search(root, 9).key)

print(delete(root, 8).key)
print(search(root, 8))
print(search(root, 7))
